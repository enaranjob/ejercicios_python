#Generadores permiten extraer varios valores dde una funcion ty almacenarlo (de uno en uno)
#en objetos iterables ()
#
"""
def generarMultiplos7(limite):
    numero = 1
    listaNumeros = []
    
    while numero <= limite:
        listaNumeros.append(numero* 7)
        numero +=1
        return listaNumeros
print(generarMultiplos7(10))
"""

def generarMultiplos7(limite):
    numero = 1
    while numero <= limite:
        yield numero *7 
        numero =+1
        
obtenerM7 = generarMultiplos7(10)

#print(obtenerM7)

for n in obtenerM7:
    print(n)