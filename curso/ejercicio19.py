#Cuando indicamos un * adelante del parametro de una funcio,
#estamos indicando que se va a recibir un numero indeterminado
#de parametros. Además, esos parametros se recibiran en frfoma de tupla 

"""
def devuelveLenguaje(*lenguaje):
    for leng in lenguaje:
        yield leng
"""

def devuelveLenguaje(*lenguaje):
    for leng in lenguaje:
        yield from leng 
lenguajesObtenidos = devuelveLenguaje("Python","Java","php","ruby","JavaScript")

print(next(lenguajesObtenidos))
print(next(lenguajesObtenidos))


