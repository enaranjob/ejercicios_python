# Break Permite salir de un bucle cuando se cumple una condicion 

"""for numero in range (1, 6):
    if numero ==3:
        break #Descanso
    print("El numero es: {0}".format(numero))
    
    
print("bucle terminado")
"""

#continue: Omite una parte del bucle cunado se cunmple una condicion y continua el resto

"""for numero in range (1, 6):
    if numero ==3:
        continue  #Continua el resto del bucle 
    print("El numero es: {0}".format(numero))
    
    
print("bucle terminado")

"""

#pass Permite continuar con una sntencia o funcion que ya no tiene o alguno no tiene bloque de codigo util

for numero in range (1, 6):
    if numero ==3:
        pass  #no pasa nadaa y el bucle sigue trabajando
    else:
        print("El siguiente valor es mayor a 3")
    print("El numero es: {0}".format(numero))
    
    
print("bucle terminado")

"""