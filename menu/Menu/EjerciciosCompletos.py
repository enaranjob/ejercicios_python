from turtle import position



class ejercicios:
    def mostrar():
        numero1 = "89"
        numero2 = "11"
        print(numero1 + numero2)

        num1 = int(numero1)
        num2 = int(numero2)
        print(num1 + num2)

        sueldo = 7890.435
        sueldoEntero = int(sueldo)
        print(sueldoEntero)

        valor = "4500.89"
        valordecimal = float(valor)
        print(valordecimal * 3)

        edad = 66998
        print(len(str(edad)))
        
        
    def mostar_operaciones(num1, num2):
        print("Suma : ", (num1 + num2))
        print("Resta : ", (num1 - num2))
        print("Multiplicación: ", (num1 * num2))
        print("División  : ", (num1 / num2))
        print("División Exacta: ", (num1 // num2))
        print("Potecia: ", (num1 ** num2))

    def mostrar_string(texto1,texto2):
        print("El saludo es: %s %s " % (texto1, texto2))
        saludofinal = "Saludo: (0) (1)".format(texto1, texto2)
        print(saludofinal)
        saludofinal = "Saludo: (0) (1)".format(texto1, texto2)
        saldofinal2 = "Saludo: (x) (y) ".format(x=texto1,y=texto2)
        print(saldofinal2)
        
    def mostrar_metodos():
        text = "esto es una oración"
        print(text)
        print(text.lower())
        print(text.upper())
        print(text.title())
        print(text.find("una"))
        print(text.count("o"))
        textEnd = text.replace("o","e")
        print(textEnd) 
        va = text.isnumeric()
        print(va)
        SeparacionCadenas = text.split(" ")
        print(SeparacionCadenas)

    def mostarTupla():
        # Tupla

        tupla = (1,2,3,"ig")
        print(tupla)

        tupla2 = (7, "Stc  uard", True , 450.2, 15 + 9j,"FElicidad,False")
        print(tupla2)

        tupla3= (9,3,(4,5,6))
        print(tupla3)

        print(tupla [-1])

        print(tupla2[0:6])

        u,o,p,q,r,a  = tupla2
        print(u,o,p,q,r,a )


        final = tupla +tupla2
        print(final)

        print(final.count(7))
        print(final.index(7))
        
        
    def mostarLitas():
        # Listas
        lista1 = ["Stuard", 20, 1, 3, True," Erlñyn", 893]
        print(lista1) 
        print(lista1[:])
        print(lista1[5])
        print(lista1[-1])


        print(lista1[0:2])
        print(lista1[:3])
        print(lista1[4:])

        lista1.append("Naranjito")
        print(lista1)

        lista1.insert(5, "Ecuador")
        print(lista1)

        lista1.extend(["Adrian", 100, False])
        print(lista1)
        
        print(lista1.index("Naranjito"))

        lista1.remove("Ecuador")
        print(lista1)

        lista1.pop()
        print(lista1)

        liusta2 = ["guayaquil", "Quito"]

        list = lista1 +liusta2
        print(list)
        print(list*8)
        print("Naranjitoo" in lista1)
        

    def mostrarDiccionario():
        dicionario = {"Ecuador":"Quito","Peru":"Lima","Alemania":"Berlin"}
        print(dicionario["Ecuador"])
        print(dicionario)

        dicionario["Venezuela"]="Caracas"
        print(dicionario)

        dicionario["Ecuador"]="Guayaquil"
        print(dicionario)

        del dicionario["Ecuador"]
        print(dicionario)

        dic2={"Stuard":"Naranjo",20:True,"Sueldo": 600}
        print(dic2["Sueldo"])

        llaves = ("España","Francia","Inglateraa")
        dicPaises = {llaves[0]:"madrid" , llaves[1]:"Paris",llaves[2]:"Londres"}
        print(dicPaises)

        datosPersonales={"Apellido":"Narannjo","años":{1:2011, 2:2012, 3:2021 ,4:2022, 5:2023}}
        print(datosPersonales)
        print(datosPersonales["años"])
        print(datosPersonales.get("Apellido","Naranjo"))

        print(datosPersonales.keys())
        valores =list(datosPersonales.values())
        print(valores)

        print(datosPersonales.keys())
        valores =tuple(datosPersonales.values())
        print(valores)
        
    def Mostrarcadena_texto(nombre, sueldo, edad):
        print("Hola, " + nombre + " y tu suelo es: ", sueldo)
        edadFutura = edad + 20 
        print("Tu edad es " , edad)
        print("En 20 años trendrás " , edadFutura)

    
    def Mostraranos(edad):
        if edad > 60:
            print("eres de tercera edad")
        elif edad > 18:
            print("eres mayor de edad")
        else:
            print("Eres menor de edad")
    
    def MostarSaludar():
        print("hola")   
        print("     mi nombre es ")
        print("                  Erlyn")
        return "                        y tengo 20 años"
    
    def mostarOpLogicos():   
        distancia = 200
        nuemeroHermanos = 3
        salarioPadres = 3000

        tineB = False
        if(distancia >1000 and nuemeroHermanos>2) or salarioPadres<2000:
            tineB = True
            
        print(not tineB)

        if(9>6) and (9<6):
            print("Verdad")
        else:
            print("Es mentira")
            
    def mostrarE():
        oficina =("Escritorio", "Silla", "Computadora")
        position = True

        descr= oficina[position + 1]
        print(descr)

        descr= oficina[position ]
        print(descr)

        descr= oficina[not position ]
        print(descr)
        
    def Mostrarango():
        numeros = range(5)
        print(numeros[1])

        numero1 = range(4, 10) 
        print(numero1[5])

        numero2 = range(10, 100, 9) 
        print(numero2[9])
        
        
    def mostrarFactorial():
        #Factorial de un numero
        numero = int(input("Ingrese un número"))
        factorial = 1
        for n in range (1, (numero + 1)):
            factorial =factorial * n
            
        print("El factorial de {0} es {1}".format(numero, factorial))
       
        
    def mostrarCicloW():
    # Ciclo While
        inicio = 2
        while inicio <= 100:
            print("numero par: {0}".format(inicio))
            inicio+=2
            
        print("Terminó el ciclo While")
        
        
    def mostrarCicloFor():
        for numero in range (1, 6):
            if numero ==3:
                pass  #no pasa nadaa y el bucle sigue trabajando
        else:
            print("El siguiente valor es mayor a 3")
            print("El numero es: {0}".format(numero))   
    print("bucle terminado")


    def mostrargenerarMultiplos7(limite):
        numero = 1
        while numero <= limite:
                yield numero *7 
                numero =+1
         
            
    def mostrardevuelveLenguaje(*lenguaje):
        for leng in lenguaje:
            yield from leng 
        
            
    def mostrarerrores():
        numero1 = 20
        numero2 = 2

        #print("la division de {0} entre {1} es:{2}".format(numero1, numero2,(numero1 / numero2)))
        try:
            resultado = numero1 /numero2
        #except
        except ZeroDivisionError:
            print("No se puede dividir entre 0....")
        finally:
            print("yo siempre aparezco.")   
        print("Aquí termina mi programa")
    
    
    
    def mostrarevaluarNota(nota):
        if nota < 0:
            raise ValueError("Valor incorrecto...")
        #raise ZeroDivisionError("No se prmeiten numeros negativos...")
        elif nota >= 16:
                print("Excelente")
        elif nota >= 11:
                print("Aprobado")
        else:
                print("Desprobado")
                
  
   

            
if __name__=="__main__":

    ejercicios.mostar_operaciones(3,3)
    ejercicios.mostrar_string("hola", "mundo")
    ejercicios.mostrar_metodos()
    ejercicios.mostrar()
    ejercicios.mostarTupla()
    ejercicios.mostarLitas()
    ejercicios.mostrarDiccionario()
    ejercicios.Mostrarcadena_texto(nombre = input("Ingrese su nombre: "), edad = int(input("Ingrese su edad: ")),sueldo = float(input("Ingresa tu sueldo ")) )
    ejercicios.Mostraranos(edad= int(input("Ingrese su edad: ")))
    ejercicios.MostrarSaludar()
    ejercicios.mostarOpLogicos()
    ejercicios.mostrarE()
    ejercicios.Mostrarango()
    ejercicios.mostrarFactorial()
    ejercicios.mostrarCicloW()
    ejercicios.mostrarCicloFor()
    ejercicios.mostrargenerarMultiplos7(15)
    ejercicios.mostrardevuelveLenguaje("Python","Java","php","ruby","JavaScript")
    ejercicios.mostrarerrores()
    ejercicios.mostrarevaluarNota(-7)
      